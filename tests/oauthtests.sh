#!/bin/bash -x

PKG="python-oauthlib"

die () {
    echo "$1" >&2
    exit 1
}

SITEDIR=`python3 -c 'import sysconfig; print(sysconfig.get_paths()["purelib"])'`
if [ "$SITEDIR" == "" ]; then
    die "Can't locate python site dir"
fi

#
# install dependencies (pytest & blinker)
#
rm -rf venv
python3 -m venv --system-site-packages --symlinks venv
. venv/bin/activate
pip install -r requirements-tests.txt

# If source not found, download it with dnf
if [ ! -d ./source ]; then
    # Extract source from srpm
    echo "## source directory not found.  attempting to download via rpm"
    dnf download --source ${PKG} && \
    rpm2cpio ${PKG}*.rpm|cpio -id && \
    mkdir source && \
    tar -zxf ${PKG}-*.tar.gz -C source --strip-components=1
    if [ $? != 0 ]; then
        echo "Failed to download upstream tests"
        exit 1
    fi
fi

# rename source code directory to be sure that we are
# testing files from our rpm
cd source || die "Missing source directory!"
if [ -d oauthlib ] ; then
    mv oauthlib oauthlib.backup || die "Can't rename/hide source code directory!"
fi
# ln -s $SITEDIR/oauthlib || die "Can't locate installed library"

#
# run upstream tests, skip jwt tests
#
echo 'import pytest; __getattr__ = lambda _: pytest.skip("this test needs jwt")' > jwt.py
pytest tests -p no:warnings --ignore tests/oauth2/rfc6749/clients/test_service_application.py | tee oauthlib.log
RESULT=${PIPESTATUS[0]}

#
#  cleanup
#
rm jwt.py
if [ -L oauthlib ] ; then
    rm oauthlib
fi
if [ -d oauthlib.backup ] ; then
    mv oauthlib.backup oauthlib
fi

exit $RESULT
